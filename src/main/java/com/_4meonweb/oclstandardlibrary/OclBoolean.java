package com._4meonweb.oclstandardlibrary;

import com._4meonweb.primitivetypes.UmlBoolean;

/** Primitive boolean type.
 *
 * @author Maxim Rodyushkin */
public interface OclBoolean
    extends OclPrimitive<Boolean, UmlBoolean, OclBoolean> {

  /** Interface defining UML primitive Boolean from Java primitive boolean and
   * String.
   *
   * @author Maxim Rodyushkin */
  public interface OclBooleanMaker
      extends OclPrimitiveMaker<Boolean, UmlBoolean, OclBoolean>, OclBoolean {
  }
}
