package com._4meonweb.oclstandardlibrary;

import com._4meonweb.primitivetypes.UmlPrimitive;

/** Common UML Primitive functionality.
 *
 * @author Maxim Rodyushkin */
public interface OclPrimitive<V, U extends UmlPrimitive<V, U>,
    P extends OclPrimitive<V, U, P>> extends OclAny<P>, UmlPrimitive<V, U> {
  /** Common functionality of UML primitive builder.
   *
   * @author Maxim Rodyushkin
   *
   * @param <P>
   *          the primitive type */
  public interface OclPrimitiveMaker<V0, U0 extends UmlPrimitive<V0, U0>,
      P0 extends OclPrimitive<V0, U0, P0>>
      extends OclPrimitive<V0, U0, P0>, UmlPrimitiveMaker<V0, U0> {

    /** Creates OCL Primitive from associated UML Primitive.
     *
     * @param value
     *          the UML primitive value
     * @return the primitive */
    P0 ofThe(U0 value);
  }

  /** Gets native Java value.
   *
   * @return the native value */
  U getUmlPrimitive();
}
