package com._4meonweb.oclstandardlibrary;

import com._4meonweb.primitivetypes.UmlBoolean;

public enum OclBooleanInvalid implements OclBoolean, OclInvalid<OclBoolean> {
  INSTANCE;

  @Override
  public UmlBoolean getUmlPrimitive() {
    return null;
  }

  @Override
  public Boolean getValue() {
    throw new UnsupportedOperationException();
  }

  @Override
  public OclBoolean oclEqual(final OclAny<OclBoolean> object2) {
    // TODO Auto-generated method stub
    return null;
  }
}
