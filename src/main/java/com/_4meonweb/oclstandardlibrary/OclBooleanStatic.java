package com._4meonweb.oclstandardlibrary;

import com._4meonweb.primitivetypes.UmlBoolean;
import com._4meonweb.primitivetypes.UmlBooleanStatic;

/** Static values of UML Boolean primitive data type.
 *
 * @author Maxim Rodyushkin */
public enum OclBooleanStatic implements OclBoolean {
  FALSE, TRUE;

  @Override
  public UmlBoolean getUmlPrimitive() {
    if (this.equals(OclBooleanStatic.FALSE)) {
      return UmlBooleanStatic.FALSE;
    }
    return UmlBooleanStatic.TRUE;
  }

  @Override
  public Boolean getValue() {
    return Boolean.parseBoolean(this.name());
  }

  @Override
  public OclBoolean oclEqual(final OclAny<OclBoolean> object2) {
    // TODO Auto-generated method stub
    return null;
  }
}
