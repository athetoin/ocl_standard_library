package com._4meonweb.oclstandardlibrary;

public interface OclInvalid<T extends OclAny<T>> extends OclAny<T> {
  default Boolean getValue() {
    throw new UnsupportedOperationException("Unsupported ");
  }

}
