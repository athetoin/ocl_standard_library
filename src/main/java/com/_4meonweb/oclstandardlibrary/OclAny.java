package com._4meonweb.oclstandardlibrary;

/** All types in the UML model and the primitive and collection types in the OCL
 * standard library conforms to the type OclAny. Conceptually, OclAny behaves as
 * a supertype for all the types. Features of OclAny are available on each
 * object in all OCL expressions. OclAny is itself an instance of the metatype
 * AnyType.
 *
 * <p>All classes in a UML model inherit all operations defined on OclAny. To
 * avoid name conflicts between properties in the model and the properties
 * inherited from OclAny, all names on the properties of OclAny start with
 * ‘ocl.’ Although theoretically there may still be name conflicts, they can
 * be avoided. One can also use qualification by OclAny (name of the type) to
 * explicitly refer to the OclAny properties.
 *
 * @author Maxim Rodyushkin */
public interface OclAny<T extends OclAny<T>> {
  /** OCL type equality.
   *
   * <p>Evaluates to invalid if object2 is invalid.
   *
   * <p>Evaluates to true if self is the same object as object2.
   *
   * <p>Evaluates to true if self and object2 are instances of the same DataType
   * and have the same value.
   *
   * <p>Evaluates to false otherwise.
   *
   * @param object2
   *          OCL object
   *
   * @return Infix operator. post: result = (self = object2) */
  OclBoolean oclEqual(final OclAny<T> object2);

  /** Check if implementations conforms to OCL Invalid type. It is false for the
   * most instances, and it supposed to be rewritten for specific invalid
   * instance of each type */
  default OclBoolean oclIsInvalid() {
    return OclBooleanStatic.FALSE;
  }
}
