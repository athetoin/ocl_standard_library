package com._4meonweb.oclstandardlibrary.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import com._4meonweb.oclstandardlibrary.OclBooleanStatic;
import com._4meonweb.primitivetypes.UmlBooleanStatic;

import org.junit.jupiter.api.Test;

class OclBooleanStaticTest {

  @Test
  void getUmlPrimitive_False_Match() {
    assertEquals(UmlBooleanStatic.FALSE,
        OclBooleanStatic.FALSE.getUmlPrimitive(),
        "FALSE values are not matched for OCL and UML");
  }

  @Test
  void getUmlPrimitive_True_Match() {
    assertEquals(UmlBooleanStatic.TRUE, OclBooleanStatic.TRUE.getUmlPrimitive(),
        "TRUE values are not matched for OCL and UML");
  }

  @Test
  void getValue_False_Match() {
    assertFalse(OclBooleanStatic.FALSE.getValue(),
        "FALSE values are not matched for OCL and Java");
  }

  @Test
  void getValue_True_Match() {
    assertTrue(OclBooleanStatic.TRUE.getValue(),
        "TRUE values are not matched for OCL and Java");
  }
}
